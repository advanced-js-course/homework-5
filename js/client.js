export function sendRequest(url, method, options) {
    return fetch(url, {method: method, ...options}).then(response => {
        if (response.ok) {
            if (response.headers.get("content-length") === "0" || !response.headers.get("content-type").startsWith("application/json")) {
                return null;
            } else {
                return response.json();
            }
        } else {
            if (response.status === 404 && method === "DELETE") {
                return null;
            }
            throw new Error('Something went wrong');
        }
    })
}

