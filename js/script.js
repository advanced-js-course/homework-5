import {sendRequest} from './client.js';
import Card from './card.js';
import Modal from './modal.js';

const cardsWrapper = document.querySelector('.cards-wrapper');
const loader = document.querySelector('.loader')

cardsWrapper.addEventListener("click", function (event) {
    if (!event.target.classList.contains("card__delete")) {
        return;
    }
    const postId = event.target.dataset["id"];
    const cardElement = event.target.closest('.card');

    sendRequest(`https://ajax.test-danit.com/api/json/posts/${postId}`, "DELETE").then(() => {
        cardElement.remove();
    })
})

cardsWrapper.addEventListener("click", function (event) {
    if (!event.target.classList.contains("card__edit")) {
        return;
    }
    const postId = event.target.dataset["id"];
    const cardElement = event.target.closest('.card');

    sendRequest(`https://ajax.test-danit.com/api/json/posts/${postId}`, "GET").then((data) => {
        const modal = new Modal(postId, data.title, data.body);
        const modalElement = modal.render();
        document.body.append(modalElement);

        modalElement.querySelector('.btn__create').addEventListener("click", (event) => {
            const title = modalElement.querySelector('input[name="title"]').value;
            const body = modalElement.querySelector('textarea[name="content"]').value;

            sendRequest(`https://ajax.test-danit.com/api/json/posts/${postId}`, "PUT", {
                body: JSON.stringify({title, body})
            }).then(async (data) => {
                const post = await sendRequest(`https://ajax.test-danit.com/api/json/posts/${postId}`, 'GET');
                const user = await sendRequest(`https://ajax.test-danit.com/api/json/users/${post.userId}`, 'GET');
                cardElement.replaceWith(new Card(postId, title, body, user.id, user.name, user.email).render());
                modal.close();
            });
        });

    });
})


const createButton = document.querySelector('.btn__create');

createButton.addEventListener("click", function (event) {
    const modal = new Modal();
    const modalElement = modal.render();
    document.body.append(modalElement);

    modalElement.querySelector('.btn__create').addEventListener("click", (event) => {
        const formData = new FormData(modalElement.querySelector('.card__form'));
        const data = Object.fromEntries(formData.entries());
        data.userId = 1;

        sendRequest("https://ajax.test-danit.com/api/json/posts", "POST", {
            body: JSON.stringify(data)
        }).then(async (data) => {
            const user = await sendRequest(`https://ajax.test-danit.com/api/json/users/${data.userId}`, 'GET');

            const card = new Card(data.id, data.title, data.content, user.id, user.name, user.email);
            cardsWrapper.insertAdjacentElement('afterbegin', card.render());
            modal.close();
        });
    });

});

function getPosts() {
    return sendRequest('https://ajax.test-danit.com/api/json/posts', 'GET').then(async posts => {
        const users = await sendRequest('https://ajax.test-danit.com/api/json/users', 'GET');
        loader.remove()

        posts.forEach(post => {
            let user = users.find(user => user.id === post.userId);
            const card = new Card(post.id, post.title, post.body, user.id, user.name, user.email);
            cardsWrapper.insertAdjacentElement('beforeend', card.render());
        })
    }).catch(error => {
        console.log(error);
    });
}

getPosts()