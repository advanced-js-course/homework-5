export default class Card {
    constructor(id, title, text, userId, username, email) {
        this._id = id;
        this._title = title;
        this._text = text;
        this._username = username;
        this._email = email;
        this._userId = userId
    }

    get userId() {
        return this._userId
    }

    render() {
        const element = document.createElement('div');
        element.classList.add("card");
        element.innerHTML = `
            <div class="card__header">
                <div class="card__author">
                    <p class="card__author--username">${this._username}</p>
                    <p class="card__author--email">${this._email}</p>
                </div>
                <div class="card__controls">
                    <div class="card__edit" data-id="${this._id}">Edit</div>
                    <div class="card__delete" data-id="${this._id}">X</div>
                </div>
            </div>
            <div class="card__content">
                <div class="content__title">
                    <h2>${this._title}</h2>
                </div>
                <div class="content__text">
                    <p>${this._text}</p>
                </div>
            </div>
        `

        return element;
    }
}