import {sendRequest} from './client.js';
import Card from './card.js';

const BODY_SCROLL_DISABLE_CLASS = 'body-scroll-disable';

export default class Modal {
    #id
    #title
    #body
    constructor(id = null, title = null, body = null) {
        this.#id = id
        this.#title = title
        this.#body = body
    }

    close() {
        document.body.classList.remove(BODY_SCROLL_DISABLE_CLASS);
        this.element.remove();
    }

    renderWrapper() {
        const wrapper = document.createElement('div');
        wrapper.classList.add("modal__wrapper");

        return wrapper;
    }

    render() {
        this.element = document.createElement('div');
        this.element.classList.add("modal");
        const wrapper = this.renderWrapper();
        wrapper.innerHTML = `
            <div class="modal__window">
                <div class="modal__header">
                    ${this.#id === null ? "Create" : "Edit"}
                </div>
                <div class="modal__content">
                    <form class="card__form">
                        <input type="text" name="title" placeholder="Title" value="${this.#title ? this.#title : ''}">
                        <textarea name="content" placeholder="Enter text...">${this.#body ? this.#body : ''}</textarea>
                    </form>
                </div>
                <div class="modal__controls">
                    <a href="#" class="btn btn__cancel">Cancel</a>
                    <a href="#" class="btn btn__create">Create</a>
                </div>
            </div>
        `

        this.element.append(wrapper);
        document.body.classList.add(BODY_SCROLL_DISABLE_CLASS);

        wrapper.addEventListener("click", (event) => {
            if (event.target.classList.contains("modal__wrapper")) {
                this.close();
            }
        })

        this.element.querySelector('.btn__cancel').addEventListener("click", (event) => {
            event.preventDefault();
            this.close();
        });


        return this.element;
    }
}